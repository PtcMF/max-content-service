import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

val jacocoVersion = "0.8.7"

plugins {
	id("org.springframework.boot") version "2.5.0"
	id("io.spring.dependency-management") version "1.0.11.RELEASE"
	id("org.sonarqube") version "3.3"
	id("jacoco")

	kotlin("jvm") version "1.5.10"
	kotlin("plugin.spring") version "1.5.10"
}

group = "org.max.lance-hotel"
version = "0.0.1-SNAPSHOT"
java.sourceCompatibility = JavaVersion.VERSION_11

repositories {
	mavenCentral()
	maven { url = uri("https://plugins.gradle.org/m2/") }
	maven { url = uri("https://jitpack.io") }
	maven {
		url = uri("https://maxmilhas-516669511250.d.codeartifact.us-east-1.amazonaws.com/maven/commons-java/")
		credentials {
			username = "aws"
			password = System.getenv("CODEARTIFACT_AUTH_TOKEN")
		}
	}

}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-webflux")
	implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor")
	implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	implementation("org.springdoc:springdoc-openapi-ui:1.5.9")
	implementation("org.springdoc:springdoc-openapi-webmvc-core:1.5.9")
	implementation("org.springdoc:springdoc-openapi-kotlin:1.5.9")
	implementation("org.max.framework:messenger-spring-boot-starter:1.0.3-SNAPSHOT")
	implementation("org.sonarsource.scanner.gradle:sonarqube-gradle-plugin:3.3")
	implementation("org.jacoco:org.jacoco.core:${jacocoVersion}")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	testImplementation("org.springframework.boot:spring-boot-starter-test")

}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "11"
	}
}

tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.jacocoTestReport {
	reports {
		xml.isEnabled = true
		html.outputLocation.set(layout.buildDirectory.dir("jacocoHtml"))
	}
}

jacoco {
	toolVersion = "${jacocoVersion}"
}

sonarqube {
	println("Root Project: ${project.projectDir}")
	properties {
		property("sonar.projectKey", "org.max:content-service")
		property("sonar.organization", "maxmilhas-ti-bitbucket")
		property( "sonar.host.url", "https://sonarcloud.io")
		property( "sonar.login", System.getenv("SONAR_CONTENT_AUTH_TOKEN"))

		property( "sonar.sources", "src/main/kotlin")
		property( "sonar.language", "java")
		property( "sonar.sourceEncoding", "UTF-8")
		property( "sonar.verbose", "true")

		property( "sonar.java.coveragePlugin", "jacoco")
		property("sonar.junit.reportPaths", "build/test-results/test")
		property("sonar.coverage.jacoco.xmlReportPaths", "${project.projectDir}/build/reports/jacoco/test/jacocoTestReport.xml")
		property("sonar.binaries" ,"build/classes/kotlin")
	}
}

tasks.named("sonarqube") {
	dependsOn(tasks.named("jacocoTestReport"))
}