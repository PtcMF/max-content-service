FROM gradle:7.0.2-jdk11 as build

ARG AWS_ACCESS_KEY_ID
ARG AWS_SECRET_ACCESS_KEY
ARG AWS_DEFAULT_REGION=us-east-1

ENV AWS_ACCESS_KEY_ID=${AWS_ACCESS_KEY_ID}
ENV AWS_SECRET_ACCESS_KEY=${AWS_SECRET_ACCESS_KEY}
ENV AWS_DEFAULT_REGION=${AWS_DEFAULT_REGION}

WORKDIR /workspace/app

RUN apt-get update; \
    apt-get -y install groff; \
    curl --silent --show-error --fail "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"; \
    unzip awscliv2.zip; \
    ./aws/install

COPY ./src src
COPY build.gradle.kts settings.gradle.kts .

RUN export CODEARTIFACT_AUTH_TOKEN=$(aws codeartifact get-authorization-token --domain maxmilhas --domain-owner 516669511250 --query authorizationToken --output text);
RUN gradle build 

FROM openjdk:11.0

ARG DEPENDENCY=/workspace/app
COPY --from=build ${DEPENDENCY}/build/libs/*.jar spring-boot-application.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "spring-boot-application.jar"]
