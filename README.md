# Content Service

## Introdução

Serviço RESTful com o objetivo de prover conteúdo a plataforma E-commerce. Neste momento,
este serviço produz conteúdo (ex. ofertas) de passagens aéreas e acomodações.

### Conteúdo aéreo

O conteúdo aéreo não está “conectado” à nossa plataforma de conteúdo aéreo, desta forma, os dados retornados por este
serviço está mocado.

| Cenário                         | SKU                                  | Descrição                                                                  |
|---------------------------------|--------------------------------------|----------------------------------------------------------------------------|
| Air Offer OW                    | 6d07a949-1c2c-41b4-b3d4-b9658faaff1a | Oferta contendo um produto aéreo, sendo apenas voo de IDA.                 |
| Air Offer RT Same Cia/Provider  | de472a93-b166-4939-a704-de740e536d5c | Oferta contendo um produto aéreo, sendo um voo RT na mesma cia/provider    |
| Air Offer RT Cross Cia/Provider | 35025334-b2e5-4100-8da5-093794854d76 | Oferta contendo dois produtos aéreos ofertados por diferentes fornecedores |
| Air Offer Expired               | f1d260ca-6038-4626-98fc-88062ec7035e | Oferta aérea expirada.                                                     |

### Conteúdo de hotelaria

Agora retornando conteúdo diretamente da API de disponibilidade do LanceHoteis. Datas e localidade
fixadas no código.

## Instalação

### Pré-requisitos

* JVM 11
* Docker

### Download do código

Clone da aplicação

    git clone git@bitbucket.org:maxmilhas/content-service.git

### Rodando a aplicação

    ./gradlew build
    java -jar build/libs/content_service-0.0.1-SNAPSHOT.jar

A aplicação estará disponível no endereço http://localhost:8080

Caso deseje rodar a aplicação utilizando docker, deve ser utilizado os passos abaixo:

Primeiro devemos fazer o build da aplicação para uma imagem docker

` docker build -t content-service --build-arg AWS_ACCESS_KEY_ID=seu_access --build-arg AWS_SECRET_ACCESS_KEY=seu_secret .
`

* Lembrando de alterar o valor de AWS_ACCESS_KEY_ID e o AWS_SECRET_ACCESS_KEY (Esses dados da aws serão usados para download das dependências)

Após isso a imagem pode ser executada

`docker run content-service -p 8080:8080`

O que estamos fazendo no comando acima é rodar uma imagem docker chamada content-service (esse nome é especificado no comando de build com o paramêtro -t). E após isso, estamos mapeando a porta exposta do container que no caso é a 8080 para a 8080 da nossa máquina.


## Documentação

* [Swagger](http://localhost:8080/swagger-ui/index.html)
* [Confluence](https://maxmilhasorg.atlassian.net/wiki/spaces/PE/pages/1667596475/Shopping+Content)

### Sonar

Para captura de dados do pelo sonar é preciso que o token de acesso ao projeto no sonar esteja na variavel de ambiente `SONAR_CONTENT_AUTH_TOKEN`:

```export SONAR_CONTENT_AUTH_TOKEN=498rd584d53a4f8ae9e9f571f29067f765ytcf2b```

Com variável registrada, agora basta executar:

```./gradlew clean build testClasses sonarqube```



.