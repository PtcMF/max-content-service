package org.max.content_service.entrypoint.api.resource.product

import org.max.content_service.entrypoint.api.resource.Adjustment
import org.max.content_service.entrypoint.api.resource.PriceSpecification
import org.max.content_service.entrypoint.api.resource.organization.Organization

class Accommodation(
    id: String,
    seller: Organization,
    rateToken: String,
    description: String,
    adjustments: List<Adjustment>,
    baseAmount: PriceSpecification,
    adjustmentsAmount: PriceSpecification,
    totalAmount: PriceSpecification,
) : Product(
    type = "@Accommodation",
    id = id,
    seller = seller,
    rateToken = rateToken,
    description = description,
    adjustments = adjustments,
    baseAmount = baseAmount,
    adjustmentsAmount = adjustmentsAmount,
    totalAmount = totalAmount
)