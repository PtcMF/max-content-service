package org.max.content_service.entrypoint.api.resource

data class PriceSpecification (
    val price: Long,
    val currency: String
)