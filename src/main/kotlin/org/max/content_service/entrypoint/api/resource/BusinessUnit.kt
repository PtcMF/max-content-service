package org.max.content_service.entrypoint.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class BusinessUnit (
    @Schema(example = "Air")
    val name: String,
    @Schema(example = "https://air-mall.max.com.br")
    val url: String? = null
)