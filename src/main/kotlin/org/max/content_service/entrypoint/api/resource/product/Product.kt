package org.max.content_service.entrypoint.api.resource.product

import io.swagger.v3.oas.annotations.media.Schema
import org.max.content_service.entrypoint.api.resource.Adjustment
import org.max.content_service.entrypoint.api.resource.PriceSpecification
import org.max.content_service.entrypoint.api.resource.organization.Organization

abstract class Product (
    @Schema(hidden = true)
    val type: String,
    @Schema(hidden = true)
    val id: String? = null,
    @Schema(hidden = true)
    val seller: Organization,
    @Schema(hidden = true)
    val rateToken: String,
    @Schema(hidden = true)
    val description: String,
    @Schema(hidden = true)
    val adjustments: List<Adjustment>,
    @Schema(hidden = true)
    val baseAmount: PriceSpecification,
    @Schema(hidden = true)
    val adjustmentsAmount: PriceSpecification,
    @Schema(hidden = true)
    val totalAmount: PriceSpecification,
)