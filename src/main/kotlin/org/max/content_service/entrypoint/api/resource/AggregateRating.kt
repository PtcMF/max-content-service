package org.max.content_service.entrypoint.api.resource

class AggregateRating (
    val reviewCount: Int,
    ratingValue: Double
) : Rating(ratingValue)