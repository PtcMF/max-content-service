package org.max.content_service.entrypoint.api.resource

open class Rating (
    val ratingValue: Double
)