package org.max.content_service.entrypoint.api.resource.payment

data class CreditCardCondition (
    val rules: List<BrandRule>
)