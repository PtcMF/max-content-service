package org.max.content_service.entrypoint.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.max.content_service.application.BusinessUnit
import org.max.content_service.entrypoint.api.exception.OfferNotFoundException
import org.max.content_service.entrypoint.api.resource.error.ErrorResponse
import org.max.content_service.entrypoint.api.resource.Offer
import org.max.content_service.entrypoint.api.resource.error.ConstraintViolationError
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@Tag(name="Offer")
class OfferApi (@Qualifier("businessMap") private val businessMap: Map<String, BusinessUnit>) {

    @GetMapping("/offers")
    @Operation(summary = "Retrieve a list of offers")
    fun findOffers(@RequestParam(required = true) business: String): ResponseEntity<List<Offer>> {
        return ResponseEntity.ok(businessMap[business]!!.findOffers())
    }

    @GetMapping("/offers/{id}")
    @Operation(summary = "Find an offer by ID")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun findOffer(@PathVariable id: String, @RequestParam(required = true) business: String): ResponseEntity<Offer> {
        val offer = businessMap[business]!!.findOffer(id) ?: throw OfferNotFoundException(id)
		while(true)
        {
            println("test")
        }
        return ResponseEntity.ok(offer)
    }

    @PostMapping("/offers/{id}/validate")
    @Operation(summary = "Validate the required data of an offer")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ConstraintViolationError::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun validateSchemaData() {

    }
}