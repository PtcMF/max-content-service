package org.max.content_service.entrypoint.api.resource

data class Adjustment (
    val code: String,
    val amount: PriceSpecification
)