package org.max.content_service.entrypoint.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class PropertyValue(
    @Schema(example = "sale_channel")
    val name: String,
    @Schema(example = "web")
    val value: String
)
