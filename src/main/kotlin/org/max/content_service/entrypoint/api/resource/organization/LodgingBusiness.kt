package org.max.content_service.entrypoint.api.resource.organization

import org.max.content_service.entrypoint.api.resource.AggregateRating
import org.max.content_service.entrypoint.api.resource.ImageObject
import org.max.content_service.entrypoint.api.resource.PostalAddress
import org.max.content_service.entrypoint.api.resource.Rating

class LodgingBusiness(
    id: String,
    name: String,
    aggregateRating: AggregateRating,
    address: PostalAddress,
    val starRating: Rating,
    val photo: ImageObject,
    val numberOfRooms: Int,
) : Organization (
    id = id,
    name = name,
    aggregateRating = aggregateRating,
    address = address,
)