package org.max.content_service.entrypoint.api.resource.organization

import io.swagger.v3.oas.annotations.media.Schema

class Airline (
    @Schema(example = "Latam")
    name: String,
    @Schema(example = "G3")
    val iataCode: String
) : Organization (
    name = name
)