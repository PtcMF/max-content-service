package org.max.content_service.entrypoint.api.resource.payment

import io.swagger.v3.oas.annotations.media.Schema

data class Installment(
    @Schema(example = "100")
    val amount: Long,
    @Schema(example = "80")
    val firstInstallmentAmount: Long,
    @Schema(example = "500")
    val totalAmount: Long,
    @Schema(example = "12")
    val number: Int,
    @Schema(example = "true")
    val hasInterest: Boolean,
    @Schema(example = "BRL")
    val currency: String
)
