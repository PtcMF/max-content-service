package org.max.content_service.entrypoint.api.resource.product.airfare

import io.swagger.v3.oas.annotations.media.Schema

data class Aircraft (
    @Schema(example = "Boeing 747")
    val name: String
)