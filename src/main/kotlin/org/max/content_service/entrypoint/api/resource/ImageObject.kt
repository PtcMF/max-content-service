package org.max.content_service.entrypoint.api.resource

data class ImageObject (
    val height: Int,
    val width: Int,
    val url: String
)