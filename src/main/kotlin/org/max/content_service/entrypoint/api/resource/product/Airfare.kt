package org.max.content_service.entrypoint.api.resource.product

import org.max.content_service.entrypoint.api.resource.Adjustment
import org.max.content_service.entrypoint.api.resource.PriceSpecification
import org.max.content_service.entrypoint.api.resource.organization.Airline
import org.max.content_service.entrypoint.api.resource.organization.Organization
import org.max.content_service.entrypoint.api.resource.product.airfare.Flight

class Airfare (
    id: String? = null,
    seller: Airline,
    provider: Organization,
    rateToken: String,
    description: String,
    adjustments: List<Adjustment>,
    baseAmount: PriceSpecification,
    adjustmentsAmount: PriceSpecification,
    totalAmount: PriceSpecification,
    val itinerary: List<Flight>,
) : Product(
    type = "@Airfare",
    id = id,
    seller = seller,
    rateToken = rateToken,
    description = description,
    adjustments = adjustments,
    baseAmount = baseAmount,
    adjustmentsAmount = adjustmentsAmount,
    totalAmount = totalAmount
)