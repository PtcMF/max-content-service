package org.max.content_service.entrypoint.api.exception

class OfferNotFoundException(val sku: String) : RuntimeException("Offer with SKU $sku was not found")