package org.max.content_service.entrypoint.api.resource.organization

import org.max.content_service.entrypoint.api.resource.AggregateRating
import org.max.content_service.entrypoint.api.resource.PostalAddress

open class Organization (
    val id: String? = null,
    val name: String,
    val aggregateRating: AggregateRating? = null,
    val address: PostalAddress? = null
)