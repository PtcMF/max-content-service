package org.max.content_service.application

import org.max.content_service.application.fixture.OneWayOffer
import org.max.content_service.application.fixture.RoundTripDifferentRateTokenOffer
import org.max.content_service.application.fixture.RoundTripSameRateTokenOffer
import org.max.content_service.application.fixture.UnavailableOffer
import org.max.content_service.entrypoint.api.resource.*
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient

@Component
class AirfareBusinessUnit : BusinessUnit {

    private val webClient: WebClient = WebClient.builder()
        .build()

    override fun findOffers(): List<Offer> = listOf(
        OneWayOffer().generate(),
        RoundTripSameRateTokenOffer().generate(),
        RoundTripDifferentRateTokenOffer().generate(),
        UnavailableOffer().generate()
    )

    override fun findOffer(id: String): Offer? {
        return findOffers().firstOrNull { it.sku == id }
    }

    override fun getName(): String = "airfare"
}
