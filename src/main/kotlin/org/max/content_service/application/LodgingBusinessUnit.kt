package org.max.content_service.application

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import org.max.content_service.application.fixture.createPaymentMethodList
import org.max.content_service.entrypoint.api.resource.*
import org.max.content_service.entrypoint.api.resource.organization.LodgingBusiness
import org.max.content_service.entrypoint.api.resource.product.Accommodation
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.WebClient
import org.springframework.web.reactive.function.client.awaitBody
import java.time.LocalDateTime
import java.util.*

@Component
class LodgingBusinessUnit : BusinessUnit {

    private val webClient: WebClient = WebClient.builder()
        .build()

    override fun findOffers(): List<Offer> {

        val dateStart = "2021-07-01"
        val dateEnd = "2021-07-08"

        val payload = mapOf(
            "place_id" to "ChIJp30aj9KQNgcR-KPi0INv3QI",
            "destination" to mapOf(
                "address" to "Porto Seguro, BA, Brasil",
                "place_id" to "ChIJp30aj9KQNgcR-KPi0INv3QI",
                "type" to "LOCALITY"
            ),
            "adults" to 1,
            "kids" to 0,
            "rooms" to arrayOf(
                mapOf(
                    "adults" to 1,
                    "kids" to 0,
                    "room_id" to "",
                    "refundable" to false,
                    "kidsAges" to emptyArray<String>()
                )
            ),
            "trip_start" to dateStart,
            "trip_end" to dateEnd,
            "hotel" to "",
            "bairro" to "",
            "city" to "undefined",
            "state" to "undefined",
            "country" to "undefined",
            "page" to 0,
            "size" to "30",
            "search_id" to "",
            "filter" to mapOf(
                "price_range" to "",
                "types" to emptyArray<String>(),
                "neighborhoods" to emptyArray<String>(),
                "name_hotel" to "",
                "refundable" to false,
                "amenities" to emptyArray<String>(),
                "stars" to emptyArray<String>(),
                "meal_plans" to emptyArray<String>()
            ),
            "sorting" to "DEFAULT",
            "lang" to "PT"
        )

        return runBlocking {
            val content = webClient
                .post()
                .uri("https://homolog2.lancehoteis.com/api/hotel/availability")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(payload)
                .retrieve()
                .awaitBody<Map<String, Any>>()

            val deferreds: ArrayList<Deferred<ArrayList<Map<String, Any>>>> = arrayListOf()
            val results:ArrayList<Map<*, *>> = arrayListOf()

            (content["search_result"] as ArrayList<*>).forEach { result ->
                val hotelId = (result as Map<*, *>)["hotel_id"]
                val detailPayload = mapOf(
                    "hotel_id" to "$hotelId",
                    "start" to dateStart,
                    "end" to dateEnd,
                    "rooms" to arrayOf(
                        mapOf(
                            "adults" to 2,
                            "kids" to 0,
                            "room_id" to "",
                            "refundable" to false,
                            "kidsAges" to emptyArray<String>()
                        )
                    ),
                    "lang" to "PT"
                )
                results.add(result)
                deferreds.add(async {
                    webClient
                        .post()
                        .uri("https://homolog2.lancehoteis.com/api/hotel/compute_room_info")
                        .bodyValue(detailPayload)
                        .retrieve()
                        .awaitBody()
                })
            }

            val offers: MutableList<Offer> = mutableListOf()

            var index = 0
            deferreds.awaitAll().forEach {
                if (it.isEmpty()) return@forEach
                offers.add(createOffer(results[index++], it.first()))
            }

            offers
        }
    }

    override fun findOffer(id: String): Offer? {
        return findOffers().firstOrNull { it.sku == id }
    }

    override fun getName(): String = "lodging"

    private fun createOffer(result: Map<*, *>, roomInfo: Map<*, *>): Offer = Offer(
        sku = result["hotel"].toString(),
        rateToken = "rate-token",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = result["name"].toString(),
        baseAmount = PriceSpecification((result["totalWithoutTax"] as Int).toLong(), currency = "BRL"),
        adjustmentsAmount = PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"),
        totalAmount = PriceSpecification((result["totalPrice"] as Int).toLong(), currency = "BRL"),
        adjustments = listOf(Adjustment("TAX", PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"))),
        availabilityStarts = LocalDateTime.now(),
        availabilityEnds = LocalDateTime.now().plusHours(1),
        products = listOf(
            Accommodation(
                id = roomInfo["room_id"].toString(),
                rateToken = roomInfo["rateKey"].toString(),
                seller = LodgingBusiness(
                    id = result["hotel_id"].toString(),
                    name = result["name"].toString(),
                    starRating = Rating((result["stars"].toString()).toDouble()),
                    aggregateRating = AggregateRating(
                        reviewCount = (result["ta_review"] as Map<*,*>)["ta_num_reviews"] as Int,
                        ratingValue = ((result["ta_review"] as Map<*,*>)["ta_rating"].toString()).toDouble(),
                    ),
                    numberOfRooms = result["remainingRooms"] as Int,
                    photo = ImageObject(
                        width = (result["cover_photo"] as Map<*,*>)["width"] as Int,
                        height = (result["cover_photo"] as Map<*,*>)["height"] as Int,
                        url = (result["cover_photo"] as Map<*,*>)["url"].toString(),
                    ),
                    address = PostalAddress(
                        postalCode = (result["address"] as Map<*, *>)["postal_code"].toString(),
                        addressCountry = (result["address"] as Map<*, *>)["country"].toString(),
                        addressState = (result["address"] as Map<*, *>)["state"].toString(),
                        addressCity = (result["address"] as Map<*, *>)["city"].toString(),
                        addressNeighborhood = (result["address"] as Map<*, *>)["bairro"].toString(),
                        streetAddress = (result["address"] as Map<*, *>)["address"].toString(),
                        addressComplement = (result["address"] as Map<*, *>)["complement"].toString(),
                    )
                ),
                description = (result["pricesSummary"] as Map<*, *>)["info"].toString(),
                baseAmount = PriceSpecification((result["totalWithoutTax"] as Int).toLong(), currency = "BRL"),
                adjustmentsAmount = PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"),
                totalAmount = PriceSpecification((result["totalPrice"] as Int).toLong(), currency = "BRL"),
                adjustments = listOf(Adjustment("TAX", PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"))),
            )
        ),
        businessUnit = BusinessUnit("lodging")
    )

    private fun createSchema(): Schema = Schema(
        name = "GuestList",
        definition = """{"type":"object","required":["guestList"],"properties":{"guestList":{"type":"array","title":"Hóspedes","items":[{"title":"Hóspede 1","type":"object","required":["firstName","lastName"],"properties":{"firstName":{"title":"Nome","type":"string","minLength":3,"maxLength":100},"lastName":{"title":"Sobrenome","type":"string","minLength":3,"maxLength":100}}}]}}}"""
    )
}
