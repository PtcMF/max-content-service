package org.max.content_service.application

import org.max.content_service.entrypoint.api.resource.Offer

interface BusinessUnit {
    fun findOffers(): List<Offer>
    fun findOffer(id: String): Offer?
    fun getName(): String
}