package org.max.content_service.infrastructure

import org.max.content_service.application.BusinessUnit
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class ApplicationConfig {

    @Bean(name = ["businessMap"])
    fun businessMap(context: ApplicationContext): Map<String, BusinessUnit> {
        val map:MutableMap<String, BusinessUnit> = mutableMapOf()

        context.getBeanNamesForType(BusinessUnit::class.java).forEach {
            val bean = context.getBean(it) as BusinessUnit
            map[bean.getName()] = bean
        }

        return map
    }
}