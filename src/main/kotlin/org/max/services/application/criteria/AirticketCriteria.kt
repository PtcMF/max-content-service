package org.max.services.application.criteria

import java.time.LocalDate

data class AirticketCriteria (
    val startDate : LocalDate,
    val endDate : LocalDate?,
): Criteria
