package org.max.services.application.criteria

import org.springdoc.core.converters.models.MonetaryAmount
import java.time.LocalDate

data class LodgingCriteria(
    val startDate: LocalDate,
    val endDate: LocalDate,
    val placeId: String?,
    val rooms: List<RoomInfo>,
    //TODO Other filters
//    val filter: Filter?,
//    val sorting: String
) : Criteria

data class RoomInfo(
    val adults : Int,
    val kidsInfo : List<KidInfo>,
)

data class KidInfo(
    val age : Int,
)

data class Filter(
    val priceRange : MonetaryAmount,
    val hotelName : String,
)
