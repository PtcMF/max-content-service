package org.max.services.application.fixture

import org.max.services.entrypoint.api.resource.*
import org.max.services.entrypoint.api.resource.organization.Airline
import org.max.services.entrypoint.api.resource.organization.Organization
import org.max.services.entrypoint.api.resource.payment.BrandRule
import org.max.services.entrypoint.api.resource.payment.CreditCardCondition
import org.max.services.entrypoint.api.resource.payment.Installment
import org.max.services.entrypoint.api.resource.payment.PaymentMethod
import org.max.services.entrypoint.api.resource.product.Airfare
import org.max.services.entrypoint.api.resource.product.airfare.Aircraft
import org.max.services.entrypoint.api.resource.product.airfare.Airport
import org.max.services.entrypoint.api.resource.product.airfare.Flight
import java.time.LocalDateTime

fun createSchema(): Schema = Schema(
    name = "PassengerList",
    definition = """{"type":"object","required":["passengers"],"properties":{"passengers":{"type":"array","title":"Passageiros","items":[{"${"$"}ref":"#/definitions/AdultPassenger","title":"Adulto 1"}]}},"definitions":{"Passenger":{"type":"object","required":["firstName","lastName","title","birthday"],"properties":{"firstName":{"title":"Nome","type":"string","minLength":3,"maxLength":100},"lastName":{"title":"Sobrenome","type":"string","minLength":3,"maxLength":100}}},"AdultPassenger":{"allOf":[{"${"$"}ref":"#/definitions/Passenger"},{"type":"object","required":["phoneNumber","documents"],"properties":{"title":{"title":"Título","enum":["sr","sra","srta"]},"phoneNumber":{"${"$"}ref":"#/definitions/PhoneNumber"},"documents":{"type":"array","minItems":2,"maxItems":2,"title":"Documentos","items":[{"allOf":[{"${"$"}ref":"#/definitions/Document"},{"properties":{"document":{"title":"CPF","type":"string","constant":"cpf"}}}]},{"allOf":[{"${"$"}ref":"#/definitions/Document"},{"required":["expirationDate"],"properties":{"document":{"title":"Passaporte","type":"string","const":"passport"},"expirationDate":{"title":"Validade passaporte","type":"string"}}}]}]}}}]},"ChildPassenger":{"allOf":[{"${"$"}ref":"#/definitions/Passenger"},{"type":"object","required":["gender"],"properties":{"title":{"title":"Título","const":"child"},"gender":{"${"$"}ref":"#/definitions/Gender"}}}]},"InfantPassenger":{"allOf":[{"${"$"}ref":"#/definitions/Passenger"},{"type":"object","required":["gender"],"properties":{"title":{"title":"Título","type":"string","constant":"infant"},"gender":{"${"$"}ref":"#/definitions/Gender"}}}]},"PhoneNumber":{"type":"object","title":"Telefone","properties":{"countryCode":{"title":"Código do país","type":"number"},"areaCode":{"title":"Código de área","type":"number"},"number":{"title":"Número","type":"string","minLength":8}}},"Gender":{"type":"string","title":"Sexo","enum":["F","M"]},"Document":{"type":"object","required":["document","number"],"properties":{"document":{"title":"Documento","type":"string"},"number":{"title":"Número do documento","type":"string"}}}}}"""
)

fun createPaymentMethodList(): List<PaymentMethod<CreditCardCondition>> {
    return listOf(
        PaymentMethod(
            paymentMethodId = "creditCard",
            paymentConditions = CreditCardCondition(
                rules = listOf(BrandRule("visa", installments = listOf(Installment(
                    amount = 100,
                    firstInstallmentAmount = 80,
                    total = 500,
                    number = 8,
                    hasInterest = true,
                    currency = "BRL"
                ))))
            )
        )
    )
}

interface AirfareFixture {
    fun generate(): Offer
}

class OneWayOffer : AirfareFixture {
    override fun generate(): Offer = Offer(
        sku = "6d07a949-1c2c-41b4-b3d4-b9658faaff1a",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = "Voo para REC saindo de CNF",
        offerToken  = "rate-token",
        subtotal = PriceSpecification(120000, "BRL"),
        adjustmentsTotal = PriceSpecification(2000, "BRL"),
        total = PriceSpecification(122000, "BRL"),
        adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
        availabilityStarts = LocalDateTime.now(),
        availabilityEnds = LocalDateTime.now().plusHours(3),
        products = listOf(
            Airfare(
                offerToken  = "rateToken",
                seller = Airline(
                    name = "Latam",
                    iataCode = "LT",
                ),
                provider = Organization(
                    name = "RX1"
                ),
                itinerary = listOf(
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        departureTime = LocalDateTime.now().plusDays(7),
                        arrivalAirport = Airport("Aeroporto de Guararapes", "REC"),
                        arrivalTime = LocalDateTime.now().plusDays(7).plusHours(3),
                        flightNumber = "LT123",
                        estimatedFlightDuration = "P1H3M"
                    )
                ),
                description = "Voo para REC saindo de CNF",
                subtotal = PriceSpecification(120000, "BRL"),
                    adjustmentsTotal = PriceSpecification(2000, "BRL"),
                    total = PriceSpecification(122000, "BRL"),
                adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
            )
        ),
        metadata = listOf(PropertyValue("foo", "bar")),
        businessUnit = BusinessUnit("airfare")
    )
}

class RoundTripSameRateTokenOffer : AirfareFixture {
    override fun generate(): Offer = Offer(
        sku = "de472a93-b166-4939-a704-de740e536d5c",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = "Voo ida e volta para REC saindo de CNF",
        offerToken  = "offer-token",
        subtotal = PriceSpecification(240000, "BRL"),
            adjustmentsTotal = PriceSpecification(4000, "BRL"),
            total = PriceSpecification(242000, "BRL"),
        adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
        availabilityStarts = LocalDateTime.now(),
        availabilityEnds = LocalDateTime.now().plusHours(3),
        products = listOf(
            Airfare(
                offerToken  = "rateToken",
                seller = Airline(
                    name = "Latam",
                    iataCode = "LT",
                ),
                provider = Organization(
                    name = "RX1"
                ),
                itinerary = listOf(
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        departureTime = LocalDateTime.now().plusDays(7),
                        arrivalAirport = Airport("Aeroporto de Guararapes", "REC"),
                        arrivalTime = LocalDateTime.now().plusDays(7).plusHours(3),
                        flightNumber = "LT123",
                        estimatedFlightDuration = "P1H3M"
                    ),
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto de Guararapes", "REC"),
                        departureTime = LocalDateTime.now().plusDays(14),
                        arrivalAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        arrivalTime = LocalDateTime.now().plusDays(14).plusHours(3),
                        flightNumber = "LT321",
                        estimatedFlightDuration = "P1H3M"
                    )
                ),
                description = "Voo ida e volta para REC saindo de CNF",
                subtotal = PriceSpecification(240000, "BRL"),
                    adjustmentsTotal = PriceSpecification(4000, "BRL"),
                    total = PriceSpecification(242000, "BRL"),
                adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
            )
        ),
        metadata = listOf(PropertyValue("foo", "bar")),
        businessUnit = BusinessUnit("airfare")
    )
}

class RoundTripDifferentRateTokenOffer : AirfareFixture {
    override fun generate(): Offer = Offer(
        sku = "35025334-b2e5-4100-8da5-093794854d76",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = "Voo ida e volta para REC saindo de CNF",
        offerToken  = "offer-token",
        subtotal = PriceSpecification(240000, "BRL"),
            adjustmentsTotal = PriceSpecification(4000, "BRL"),
            total = PriceSpecification(242000, "BRL"),
        adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
        availabilityStarts = LocalDateTime.now(),
        availabilityEnds = LocalDateTime.now().plusHours(3),
        products = listOf(
            Airfare(
                offerToken  = "rateToken",
                seller = Airline(
                    name = "Latam",
                    iataCode = "LT",
                ),
                provider = Organization(
                    name = "RX1"
                ),
                itinerary = listOf(
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        departureTime = LocalDateTime.now().plusDays(7),
                        arrivalAirport = Airport("Aeroporto de Guararapes", "REC"),
                        arrivalTime = LocalDateTime.now().plusDays(7).plusHours(3),
                        flightNumber = "LT123",
                        estimatedFlightDuration = "P1H3M"
                    )
                ),
                description = "Voo para REC saindo de CNF",
                subtotal = PriceSpecification(120000, "BRL"),
                    adjustmentsTotal = PriceSpecification(2000, "BRL"),
                    total = PriceSpecification(122000, "BRL"),
                adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
            ),
            Airfare(
                offerToken  = "rateToken",
                seller = Airline(
                    name = "Latam",
                    iataCode = "LT",
                ),
                provider = Organization(
                    name = "RX1"
                ),
                itinerary = listOf(
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto de Guararapes", "REC"),
                        departureTime = LocalDateTime.now().plusDays(14),
                        arrivalAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        arrivalTime = LocalDateTime.now().plusDays(14).plusHours(3),
                        flightNumber = "LT321",
                        estimatedFlightDuration = "P1H3M"
                    )
                ),
                description = "Voo para CNF saindo de REC",
                subtotal = PriceSpecification(120000, "BRL"),
                    adjustmentsTotal = PriceSpecification(2000, "BRL"),
                    total = PriceSpecification(122000, "BRL"),
                adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
            )
        ),
        metadata = listOf(PropertyValue("foo", "bar")),
        businessUnit = BusinessUnit("airfare")
    )
}

class UnavailableOffer : AirfareFixture {
    override fun generate(): Offer = Offer(
        sku = "f1d260ca-6038-4626-98fc-88062ec7035e",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = "[UNAVAILABLE] Voo para REC saindo de CNF",
        offerToken  = "offer-token",
        subtotal = PriceSpecification(120000, "BRL"),
            adjustmentsTotal = PriceSpecification(2000, "BRL"),
            total = PriceSpecification(122000, "BRL"),
        adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
        availabilityStarts = LocalDateTime.now().minusHours(5),
        availabilityEnds = LocalDateTime.now().minusHours(3),
        products = listOf(
            Airfare(
                offerToken  = "rateToken",
                seller = Airline(
                    name = "Latam",
                    iataCode = "LT",
                ),
                provider = Organization(
                    name = "RX1"
                ),
                itinerary = listOf(
                    Flight(
                        aircraft = Aircraft("Boing 747"),
                        airline = Airline("Latam", "LT"),
                        departureAirport = Airport("Aeroporto internacional de Confins", "CNF"),
                        departureTime = LocalDateTime.now().plusDays(7),
                        arrivalAirport = Airport("Aeroporto de Guararapes", "REC"),
                        arrivalTime = LocalDateTime.now().plusDays(7).plusHours(3),
                        flightNumber = "LT123",
                        estimatedFlightDuration = "P1H3M"
                    )
                ),
                description = "Voo para REC saindo de CNF",
                subtotal = PriceSpecification(120000, "BRL"),
                    adjustmentsTotal = PriceSpecification(2000, "BRL"),
                total = PriceSpecification(122000, "BRL"),
                adjustments = listOf(Adjustment("FEE_SERVICE", PriceSpecification(2000, "BRL"))),
            )
        ),
        metadata = listOf(PropertyValue("foo", "bar")),
        businessUnit = BusinessUnit("airfare")
    )
}