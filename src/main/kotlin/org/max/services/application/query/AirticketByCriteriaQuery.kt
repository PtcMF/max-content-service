package org.max.services.application.query

import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate

data class AirticketByCriteriaQuery (
    @Schema(example = "2021-08-27")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val startDate : LocalDate,
    @Schema(example = "2021-08-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val endDate : LocalDate?,
)
