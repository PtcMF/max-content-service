package org.max.services.application.query

import io.swagger.v3.oas.annotations.media.Schema
import org.springframework.format.annotation.DateTimeFormat
import java.time.LocalDate

data class LodgingByCriteriaQuery (
    @Schema(example = "2021-08-27")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val checkInDate : LocalDate,
    @Schema(example = "2021-08-30")
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    val checkOutDate : LocalDate,
    @Schema(example = "ChIJ0WGkg4FEzpQRrlsz_whLqZs")
    val placeId : String?,
    val rooms : List<RoomInfo>?,
)

data class RoomInfo(
    @Schema(example = "2")
    val adults : Int,
    val kidsInfo : List<KidInfo>?,
)
data class KidInfo(
    @Schema(example = "6")
    val age : Int,
)