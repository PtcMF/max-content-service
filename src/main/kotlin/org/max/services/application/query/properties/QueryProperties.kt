package org.max.services.application.query.properties

data class QueryProperties (
    var lanceHoteis: MutableMap<String, String> = mutableMapOf(),
    var otherProvider: MutableMap<String, String> = mutableMapOf(),
)


