package org.max.services.application.mapper

import org.max.services.application.criteria.AirticketCriteria
import org.max.services.application.criteria.LodgingCriteria
import org.max.services.application.query.AirticketByCriteriaQuery
import org.max.services.application.query.LodgingByCriteriaQuery
import org.springframework.stereotype.Component

@Component
class CriteriaMapper () {
    fun createLodgingCriteria(criteria: LodgingByCriteriaQuery): LodgingCriteria {
        return LodgingCriteria(
            startDate =  criteria.checkInDate,
            endDate = criteria.checkOutDate,
            placeId = criteria.placeId,
            rooms = criteria.rooms.orEmpty().map { org.max.services.application.criteria.RoomInfo(
                adults = it.adults,
                kidsInfo = it.kidsInfo.orEmpty().map {it1 -> org.max.services.application.criteria.KidInfo(
                    age = it1.age
                ) }
            ) },
            //TODO Demais filtros a serem implementados
//            filter = (criteria.filter !== null) ? Filter(priceRange = Money.of(criteria.filter.priceRange, "BRL"), hotelName = "") : null,
//            sorting = "DEFAULT"

        )
    }
    fun createAirticketCriteria(criteria: AirticketByCriteriaQuery): AirticketCriteria {
        return AirticketCriteria(
            startDate =  criteria.startDate,
            endDate = criteria.endDate,
        )
    }
}