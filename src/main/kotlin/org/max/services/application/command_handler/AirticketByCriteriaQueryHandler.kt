package org.max.services.application.command_handler

import org.max.services.application.query.AirticketByCriteriaQuery
import org.max.services.domain.repository.AirticketRepository
import org.max.services.application.mapper.CriteriaMapper
import org.max.services.entrypoint.api.resource.Offer
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component


@Component
class AirticketByCriteriaQueryHandler(
    val repository: AirticketRepository,
    val criteriaFactory: CriteriaMapper,
    val eventBus: MessageBus
) {

    @Handler("commandBus")
    fun handle(command: Envelope<AirticketByCriteriaQuery>): List<Offer> {
        val criteria =  criteriaFactory.createAirticketCriteria(command.message)
        return repository.findOffers(criteria)
    }
}