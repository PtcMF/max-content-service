package org.max.services.application.command_handler

import org.max.services.application.query.LodgingByCriteriaQuery
import org.max.services.domain.repository.LodgingRepository
import org.max.services.application.mapper.CriteriaMapper
import org.max.services.entrypoint.api.resource.Offer
import org.max.messenger.core.Envelope
import org.max.messenger.core.MessageBus
import org.max.messenger.core.annotation.Handler
import org.springframework.stereotype.Component


@Component
class LodgingByCriteriaQueryHandler(
    val lodgingRepository: LodgingRepository,
    val criteriaFactory: CriteriaMapper,
    val eventBus: MessageBus
) {

    @Handler("commandBus")
    fun handle(command: Envelope<LodgingByCriteriaQuery>): List<Offer> {
        val criteria = criteriaFactory.createLodgingCriteria(command.message)
        return lodgingRepository.findOffers(criteria)

    }
}