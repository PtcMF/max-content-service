package org.max.services.entrypoint.api.resource

import io.swagger.v3.oas.annotations.media.Schema

data class Schema (
    @Schema(example = "GuestList")
    val name: String,
    @Schema(example = "{}", description = "JSONSchema7 definition")
    val definition: String
)