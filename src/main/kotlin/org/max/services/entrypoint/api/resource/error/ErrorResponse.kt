package org.max.services.entrypoint.api.resource.error

import io.swagger.v3.oas.annotations.media.Schema

class ErrorResponse(
    @Schema(example = "404")
    val code: Int,
    @Schema(example = "ResourceNotFound")
    val type: String,
    @Schema(example = "The resource requested was not found")
    val message: String,
    @Schema(example = "{}", description = "An arbitrary set of data")
    val details: Map<String, Any>? = null
)