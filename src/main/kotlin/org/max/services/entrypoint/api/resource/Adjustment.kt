package org.max.services.entrypoint.api.resource

data class Adjustment (
    val code: String,
    val amount: PriceSpecification
)