package org.max.services.entrypoint.api.resource.payment

import io.swagger.v3.oas.annotations.media.Schema

data class PaymentMethod <T> (
    @Schema(example = "creditCard")
    val paymentMethodId: String,
    val paymentConditions: T
)
