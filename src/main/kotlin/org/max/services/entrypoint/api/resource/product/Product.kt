package org.max.services.entrypoint.api.resource.product

import io.swagger.v3.oas.annotations.media.Schema
import org.max.services.entrypoint.api.resource.Adjustment
import org.max.services.entrypoint.api.resource.PriceSpecification
import org.max.services.entrypoint.api.resource.organization.Organization

abstract class Product (
    @Schema(hidden = true)
    val type: String,
    @Schema(hidden = true)
    val id: String? = null,
    @Schema(hidden = true)
    val seller: Organization,
    @Schema(hidden = true)
    val offerToken : String,
    @Schema(hidden = true)
    val description: String,
    @Schema(hidden = true)
    val adjustments: List<Adjustment>,
    @Schema(hidden = true)
    val subtotal: PriceSpecification,
    @Schema(hidden = true)
    val adjustmentsTotal: PriceSpecification,
    @Schema(hidden = true)
    val total: PriceSpecification,
)