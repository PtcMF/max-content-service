package org.max.services.entrypoint.api.resource

import com.fasterxml.jackson.annotation.JsonFormat
import org.max.services.entrypoint.api.resource.payment.CreditCardCondition
import org.max.services.entrypoint.api.resource.payment.PaymentMethod
import org.max.services.entrypoint.api.resource.product.Product
import org.max.services.entrypoint.api.resource.product.Accommodation
import org.max.services.entrypoint.api.resource.product.Airfare
import java.time.LocalDateTime
import io.swagger.v3.oas.annotations.media.Schema as ApiSchema

data class Offer(
    @ApiSchema(example = "ebcfc227-b409-4258-8569-0f8297fd8f16")
    val sku: String,
    val schemas: List<Schema>? = null,
    val paymentMethods: List<PaymentMethod<CreditCardCondition>>,
    @ApiSchema(anyOf = [Airfare::class, Accommodation::class])
    val products: List<Product>,
    @ApiSchema(example = "Passagem aérea BHZ <> REC")
    val description: String,
    val subtotal: PriceSpecification,
    val adjustmentsTotal: PriceSpecification,
    val total: PriceSpecification,
    val adjustments: List<Adjustment>,

    @ApiSchema(example = "2022-01-01T12:00:00")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val availabilityStarts: LocalDateTime,

    @ApiSchema(example = "2022-01-01T15:00:00")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val availabilityEnds: LocalDateTime,

    @ApiSchema(example = "some-rate-token")
    val offerToken: String,
    @ApiSchema(description = "List of arbitrary set of data")
    val metadata: List<PropertyValue>? = emptyList(),
    val businessUnit: BusinessUnit
)
