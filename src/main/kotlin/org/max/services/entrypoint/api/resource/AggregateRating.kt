package org.max.services.entrypoint.api.resource

class AggregateRating (
    val reviewCount: Int,
    ratingValue: Double
) : Rating(ratingValue)