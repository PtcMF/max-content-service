package org.max.services.entrypoint.api.resource.payment

data class CreditCardCondition (
    val rules: List<BrandRule>
)