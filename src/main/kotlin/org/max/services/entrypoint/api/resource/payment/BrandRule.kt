package org.max.services.entrypoint.api.resource.payment

import io.swagger.v3.oas.annotations.media.Schema

data class BrandRule(
    @Schema(example = "mastercard")
    val brand: String,
    val installments: List<Installment>
)
