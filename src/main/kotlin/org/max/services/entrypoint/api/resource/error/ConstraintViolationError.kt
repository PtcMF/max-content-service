package org.max.services.entrypoint.api.resource.error

import io.swagger.v3.oas.annotations.media.Schema

class ConstraintViolationError(
    @Schema(example = "400")
    val code: Int,
    @Schema(example = "ConstraintViolationError")
    val type: String,
    @Schema(example = "The given data has one or more constraint violations")
    val message: String,
    @Schema()
    val violations: List<ConstraintViolation>
)