package org.max.services.entrypoint.api.resource.error

import io.swagger.v3.oas.annotations.media.Schema

class ConstraintViolation (
    @Schema(example = "passengerList[0].firstName")
    val path: String,
    @Schema(example = "First name cannot be blank")
    val message: String,
    @Schema(example = "NOT_BLANK")
    val code: String
)