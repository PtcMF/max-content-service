package org.max.services.entrypoint.api.resource.product.airfare

import io.swagger.v3.oas.annotations.media.Schema

class Airport (
    @Schema(example = "Aeroporto Internacional de Confins")
    val name: String,
    @Schema(example = "CNF")
    val iataCode: String
)