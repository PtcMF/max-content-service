package org.max.services.entrypoint.api.resource.product.airfare

import com.fasterxml.jackson.annotation.JsonFormat
import io.swagger.v3.oas.annotations.media.Schema
import org.max.services.entrypoint.api.resource.organization.Airline
import java.time.LocalDateTime

class Flight (
    val itinerary: List<Flight>? = null,
    val aircraft: Aircraft,
    val airline: Airline,
    val departureAirport: Airport,
    @Schema(example = "2017-03-04T20:15:00")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val departureTime: LocalDateTime,
    val arrivalAirport: Airport,
    @Schema(example = "2017-03-04T20:15:00")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val arrivalTime: LocalDateTime,
    @Schema(example = "G1043")
    val flightNumber: String,
    @Schema(example = "P1H3M")
    val estimatedFlightDuration: String,
)