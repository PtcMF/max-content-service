package org.max.services.entrypoint.api.resource

data class ImageObject (
    val height: Int,
    val width: Int,
    val url: String
)