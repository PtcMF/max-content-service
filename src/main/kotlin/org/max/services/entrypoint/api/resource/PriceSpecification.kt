package org.max.services.entrypoint.api.resource

data class PriceSpecification (
    val price: Long,
    val currency: String
)