package org.max.services.entrypoint.api.exception

class OfferNotFoundException(val sku: String) : RuntimeException("Offer with SKU $sku was not found")