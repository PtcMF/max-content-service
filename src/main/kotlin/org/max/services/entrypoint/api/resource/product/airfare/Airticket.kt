package org.max.services.entrypoint.api.resource.product.airfare

import com.fasterxml.jackson.annotation.JsonFormat
import io.swagger.v3.oas.annotations.media.Schema
import java.time.LocalDateTime

data class Airticket (
    @Schema(example = "JJ 1151063107")
    val ticketNumber: String,
    @Schema(example = "http://")
    val ticketToken: String,
    @Schema(example = "Ícaro da Silva Mattos")
    val underName: String,
    @Schema(example = "2017-03-04T20:15:00")
    @JsonFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss")
    val departureTime: LocalDateTime,
    val flight: Flight
)