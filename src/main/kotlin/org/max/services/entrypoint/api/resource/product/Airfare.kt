package org.max.services.entrypoint.api.resource.product

import org.max.services.entrypoint.api.resource.Adjustment
import org.max.services.entrypoint.api.resource.PriceSpecification
import org.max.services.entrypoint.api.resource.organization.Airline
import org.max.services.entrypoint.api.resource.organization.Organization
import org.max.services.entrypoint.api.resource.product.airfare.Flight

class Airfare (
    id: String? = null,
    seller: Airline,
    provider: Organization,
    offerToken : String,
    description: String,
    adjustments: List<Adjustment>,
    subtotal: PriceSpecification,
    adjustmentsTotal: PriceSpecification,
    total: PriceSpecification,
    val itinerary: List<Flight>,
) : Product(
    type = "@Airfare",
    id = id,
    seller = seller,
    offerToken  = offerToken ,
    description = description,
    adjustments = adjustments,
    subtotal = subtotal,
    adjustmentsTotal = adjustmentsTotal,
    total = total
)