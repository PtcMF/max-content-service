package org.max.services.entrypoint.api.resource.organization

import org.max.services.entrypoint.api.resource.AggregateRating
import org.max.services.entrypoint.api.resource.PostalAddress

class AirfareBusiness (
    id: String? = null,
    name: String,
    aggregateRating: AggregateRating? = null,
    address: PostalAddress? = null,
) : Organization (
    id = id,
    name = name,
    aggregateRating = aggregateRating,
    address = address,
)