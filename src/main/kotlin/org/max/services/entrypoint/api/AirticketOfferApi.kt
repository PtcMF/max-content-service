package org.max.services.entrypoint.api

import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.Parameter
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.max.services.application.query.AirticketByCriteriaQuery
import org.max.services.entrypoint.api.resource.Offer
import org.max.services.entrypoint.api.resource.error.ErrorResponse
import org.max.messenger.core.MessageBus
import org.max.messenger.core.stamp.HandledStamp
import org.max.services.application.query.LodgingByCriteriaQuery
import org.springframework.format.annotation.DateTimeFormat
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import org.springframework.web.context.request.WebRequest
import java.time.LocalDate

@RestController
@Tag(name="AirticketOffer")
class AirticketOfferApi (
    private val commandBus: MessageBus,
    ) {

    @PostMapping("/offers/airticket", consumes = ["application/json"],  produces = ["application/json"])
    @Operation(summary = "Retrieve a list of airticket offers")
    @ApiResponses(
        ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "201"),
    )
    fun findOffers(
        @Parameter(description = "Amount rooms", example = "1")
        @RequestBody criteria : AirticketByCriteriaQuery,
        request: WebRequest
    ): ResponseEntity<List<Offer>> {
        val envelope = commandBus.dispatch(criteria)
        val offers = envelope.lastOf<HandledStamp>()!!.result as List<Offer>
        return ResponseEntity(offers, HttpStatus.OK)
    }

    @GetMapping("/offers/airticket/{id}")
    @Operation(summary = "Find lodging offers")
    @ApiResponses(
        ApiResponse(responseCode = "404", content = [Content(schema = Schema(implementation = ErrorResponse::class))]),
        ApiResponse(responseCode = "200"),
    )
    fun findOffer(@RequestParam(required = true) id: String): ResponseEntity<Offer> {
        TODO("Not yet implemented")
        throw Exception("Not yet implemented")
//        val offer = businessMap[business]!!.findOffer(id) ?: throw OfferNotFoundException(id)
//        return ResponseEntity.ok(offer)
    }
}