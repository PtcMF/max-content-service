package org.max.services.entrypoint.api.resource.product

import org.max.services.entrypoint.api.resource.Adjustment
import org.max.services.entrypoint.api.resource.PriceSpecification
import org.max.services.entrypoint.api.resource.organization.Organization

class Accommodation(
    id: String,
    seller: Organization,
    offerToken : String,
    description: String,
    adjustments: List<Adjustment>,
    subtotal: PriceSpecification,
    adjustmentsTotal: PriceSpecification,
    total: PriceSpecification,
) : Product(
    type = "@Accommodation",
    id = id,
    seller = seller,
    offerToken = offerToken ,
    description = description,
    adjustments = adjustments,
    subtotal = subtotal,
    adjustmentsTotal = adjustmentsTotal,
    total = total
)