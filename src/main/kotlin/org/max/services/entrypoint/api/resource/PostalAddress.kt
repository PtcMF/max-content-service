package org.max.services.entrypoint.api.resource

data class PostalAddress (
    val addressCountry: String,
    val addressState: String,
    val addressCity: String,
    val addressNeighborhood: String,
    val streetAddress: String,
    val postalCode: String,
    val addressComplement: String? = null,
)