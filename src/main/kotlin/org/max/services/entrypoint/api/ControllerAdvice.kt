package org.max.services.entrypoint.api

import org.max.services.entrypoint.api.exception.OfferNotFoundException
import org.max.services.entrypoint.api.resource.error.ErrorResponse
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ControllerAdvice {

    @ExceptionHandler(value = [OfferNotFoundException::class])
    fun handle(ex: OfferNotFoundException): ResponseEntity<ErrorResponse> {
        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(ErrorResponse(
            code = HttpStatus.NOT_FOUND.value(),
            type = ex::class.simpleName!!,
            message = ex.message!!
        ))
    }
}