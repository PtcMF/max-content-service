package org.max.services.entrypoint.api.resource

open class Rating (
    val ratingValue: Double
)