package org.max.services.infrastructure

import org.max.services.domain.repository.properties.RepositoryProperties
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class ApplicationConfig {

    @Bean
    @ConfigurationProperties(prefix = "repository.http")
    fun queryProperties(): RepositoryProperties? {
        return RepositoryProperties()
    }
}