package org.max.services.domain.repository

import org.max.services.application.criteria.Criteria
import org.max.services.entrypoint.api.resource.Offer
import org.springframework.web.reactive.function.client.WebClient

interface OfferRepository<T:Criteria> {
    val webClient: WebClient
        get() = WebClient.builder().build()

    fun findOffers(criteria: T): List<Offer>
    fun findOffer(sku: String): Offer?
    fun getRepositoryName(): String
    {
        return this::class.simpleName!!.replace("Repository","")
    }
}