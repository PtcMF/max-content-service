package org.max.services.domain.repository

import org.max.services.application.criteria.AirticketCriteria
import org.max.services.application.fixture.OneWayOffer
import org.max.services.application.fixture.RoundTripDifferentRateTokenOffer
import org.max.services.application.fixture.RoundTripSameRateTokenOffer
import org.max.services.application.fixture.UnavailableOffer
import org.max.services.entrypoint.api.resource.Offer
import org.springframework.stereotype.Component
import java.time.LocalDate

@Component
class AirticketRepository() : OfferRepository<AirticketCriteria> {

    override fun findOffer(sku: String): Offer? {
        return findOffers(AirticketCriteria(startDate = LocalDate.now(), null)).firstOrNull { it.sku == sku }
    }

    override fun findOffers(criteria: AirticketCriteria): List<Offer> {
        RoundTripSameRateTokenOffer().generate()
        RoundTripDifferentRateTokenOffer().generate()
        UnavailableOffer().generate()
        return listOf(OneWayOffer().generate())
    }
}
