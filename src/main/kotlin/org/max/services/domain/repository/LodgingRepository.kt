package org.max.services.domain.repository

import kotlinx.coroutines.Deferred
import kotlinx.coroutines.async
import kotlinx.coroutines.awaitAll
import kotlinx.coroutines.runBlocking
import org.max.services.application.criteria.LodgingCriteria
import org.max.services.application.fixture.createPaymentMethodList
import org.max.services.domain.repository.properties.RepositoryProperties
import org.max.services.entrypoint.api.resource.*
import org.max.services.entrypoint.api.resource.organization.LodgingBusiness
import org.max.services.entrypoint.api.resource.product.Accommodation
import org.springframework.http.MediaType
import org.springframework.stereotype.Component
import org.springframework.web.reactive.function.client.awaitBody
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

@Component
class LodgingRepository(
    val queryProperties : RepositoryProperties
) : OfferRepository<LodgingCriteria> {
    var dateFormat: DateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")

    override fun findOffer(id: String): Offer? {
        return null
    }

    override fun findOffers(criteria: LodgingCriteria): List<Offer> {
        return findLanceHoteisOffers(criteria)
    }

    //Busca Ofertas do Lance Hoteis
    fun findLanceHoteisOffers(criteria: LodgingCriteria): List<Offer> {

        val payload = createAvailabilityPayload(criteria)
        return runBlocking {
            val content = webClient
                .post()
                .uri(queryProperties.lanceHoteis.get("availabilityUrl")!!)
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(payload)
                .retrieve()
                .awaitBody<Map<String, Any>>()

            val deferreds: ArrayList<Deferred<ArrayList<Map<String, Any>>>> = arrayListOf()
            val results:ArrayList<Map<*, *>> = arrayListOf()

            (content["search_result"] as ArrayList<*>).forEach { result ->
                val hotelId = (result as Map<*, *>)["hotel_id"]
                val detailPayload = createRoomDetailPayload(hotelId as String, criteria)
                results.add(result)
                deferreds.add(async {
                    webClient
                        .post()
                        .uri(queryProperties.lanceHoteis.get("roomInfoUrl")!!)
                        .bodyValue(detailPayload)
                        .retrieve()
                        .awaitBody()
                })
            }

            val offers: MutableList<Offer> = mutableListOf()

            var index = 0
            deferreds.awaitAll().forEach {
                if (it.isEmpty()) return@forEach
                offers.add(createOffer(results[index++], it.first()))
            }

            offers
        }
    }
    fun createAvailabilityPayload(criteria: LodgingCriteria): Map<String, Any?> {
        var adults : Int = 0
        var kids : Int = 0
        return listOfNotNull(
            "placeID" to criteria.placeId,
            "destination" to mapOf(
                "type" to "LOCALITY",
                "address" to "São Paulo, SP, Brasil",
                "place_id" to criteria.placeId,
            ),
            "rooms" to criteria.rooms.map {
                adults += it.adults
                kids += it.kidsInfo.size
                mapOf(
                    "adults" to it.adults,
                    "kids" to it.kidsInfo.size,
                    "room_id" to "",
                    "refundable" to false,
                    "kidsAges" to it.kidsInfo.map { info -> info.age.toString() }.toList()
            )},
            "adults" to adults,
            "kids" to kids,
            "trip_start" to criteria.startDate.format(dateFormat),
            "trip_end" to criteria.endDate.format(dateFormat),
            "hotel" to "",
            "bairro" to "",
            "city" to "curitiba",
            "state" to "undefined",
            "country" to "undefined",
            "page" to 0,
            "size" to "50",
            "search_id" to "",
            "filter" to mapOf(
                "price_range" to "",
                "types" to emptyArray<String>(),
                "neighborhoods" to emptyArray<String>(),
                "name_hotel" to "",
                "refundable" to false,
                "amenities" to emptyArray<String>(),
                "stars" to emptyArray<String>(),
                "meal_plans" to emptyArray<String>()
            ),
            "sorting" to "DEFAULT",
            "neighborhood_chosen" to "",
            "hotel_chosen" to "",
            "lang" to "PT"
        ).toMap()
    }
    fun createRoomDetailPayload(hotelId : String, criteria: LodgingCriteria): Map<String, Any> {
        return mapOf(
            "hotel_id" to "$hotelId",
            "start" to criteria.startDate.format(dateFormat),
            "end" to criteria.endDate.format(dateFormat),
            "rooms" to arrayOf(
                mapOf(
                    "adults" to 2,
                    "kids" to 0,
                    "room_id" to "",
                    "refundable" to false,
                    "kidsAges" to emptyArray<String>()
                )
            ),
            "lang" to "PT"
        )
    }
    private fun createOffer(result: Map<*, *>, roomInfo: Map<*, *>): Offer = Offer(
        sku = result["hotel"].toString(),
        offerToken  = "offer-token",
        schemas = listOf(createSchema()),
        paymentMethods = createPaymentMethodList(),
        description = result["name"].toString(),
        subtotal = PriceSpecification((result["totalWithoutTax"] as Int).toLong(), currency = "BRL"),
            adjustmentsTotal = PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"),
        total = PriceSpecification((result["totalPrice"] as Int).toLong(), currency = "BRL"),
        adjustments = listOf(Adjustment("TAX", PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"))),
        availabilityStarts = LocalDateTime.now(),
        availabilityEnds = LocalDateTime.now().plusHours(1),
        products = listOf(
            Accommodation(
                id = roomInfo["room_id"].toString(),
                offerToken  = roomInfo["rateKey"].toString(),
                seller = LodgingBusiness(
                    id = result["hotel_id"].toString(),
                    name = result["name"].toString(),
                    starRating = Rating((result["stars"].toString()).toDouble()),
                    aggregateRating = AggregateRating(
                        reviewCount = (result["ta_review"] as Map<*,*>)["ta_num_reviews"] as Int,
                        ratingValue = ((result["ta_review"] as Map<*,*>)["ta_rating"].toString()).toDouble(),
                    ),
                    numberOfRooms = result["remainingRooms"] as Int,
                    photo = ImageObject(
                        width = (result["cover_photo"] as Map<*,*>)["width"] as Int,
                        height = (result["cover_photo"] as Map<*,*>)["height"] as Int,
                        url = (result["cover_photo"] as Map<*,*>)["url"].toString(),
                    ),
                    address = PostalAddress(
                        postalCode = (result["address"] as Map<*, *>)["postal_code"].toString(),
                        addressCountry = (result["address"] as Map<*, *>)["country"].toString(),
                        addressState = (result["address"] as Map<*, *>)["state"].toString(),
                        addressCity = (result["address"] as Map<*, *>)["city"].toString(),
                        addressNeighborhood = (result["address"] as Map<*, *>)["bairro"].toString(),
                        streetAddress = (result["address"] as Map<*, *>)["address"].toString(),
                        addressComplement = (result["address"] as Map<*, *>)["complement"].toString(),
                    )
                ),
                description = (result["pricesSummary"] as Map<*, *>)["info"].toString(),
                subtotal = PriceSpecification((result["totalWithoutTax"] as Int).toLong(), currency = "BRL"),
                    adjustmentsTotal = PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"),
                total = PriceSpecification((result["totalPrice"] as Int).toLong(), currency = "BRL"),
                adjustments = listOf(Adjustment("TAX", PriceSpecification((result["totalTax"] as Int).toLong(), currency = "BRL"))),
            )
        ),
        businessUnit = BusinessUnit("lodging")
    )

    private fun createSchema(): Schema = Schema(
        name = "GuestList",
        definition = """{"type":"object","required":["guestList"],"properties":{"guestList":{"type":"array","title":"Hóspedes","items":[{"title":"Hóspede 1","type":"object","required":["firstName","lastName"],"properties":{"firstName":{"title":"Nome","type":"string","minLength":3,"maxLength":100},"lastName":{"title":"Sobrenome","type":"string","minLength":3,"maxLength":100}}}]}}}"""
    )

}
