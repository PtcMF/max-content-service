package org.max.services.domain.repository.properties

data class RepositoryProperties (
    var lanceHoteis: MutableMap<String, String> = mutableMapOf(),
    var otherExample: MutableMap<String, String> = mutableMapOf(),
)


